# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Tax Cash Based DATEV',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Steuer ' \
            'Ist-Versteuerung DATEV',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''
    - DATEV functionality for cash based accounting in Germany
''',
    'description_de_DE': '''
    - DATEV Funktionalität für Deutschland im Zusammenhang mit IST-Versteuerung
''',
    'depends': [
        'account_tax_cash_basis',
        'account_timeline_datev',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
