# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class MoveDatev(ModelSQL, ModelView):
    _name = 'account.move.datev'

    def _hook_skip_line(self, line):
        if self._check_payment_tax(line):
                return True
        else:
            return super(MoveDatev, self)._hook_skip_line(line)

    def _check_payment_tax(self, line):
        res = False
        if line.tax_lines:
            if len(line.tax_lines) == 1 and line.tax_lines[0].tax.payment_tax:
                res = True
        return res

    def _get_taxes(self, line):
        '''
        With cash_income we only use the parent of the tax of the tax line for
        the respective lines
        '''
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = False
        fiscalyear = fiscalyear_obj.get_fiscalyear(date=line.move.date)
        # TODO: check, if kind others must also be handled this way (issue2277)
        if (fiscalyear.taxation_method == 'cash_income' and
            line.account.kind == 'revenue'):
            if line.tax_lines:
                res = []
                for tax_line in line.tax_lines:
                    if tax_line.tax.parent:
                        if tax_line.tax.parent.id not in res:
                            res.append(tax_line.tax.parent.id)
                res = tuple(res)
        else:
            res = super(MoveDatev, self)._get_taxes(line)
        return res

MoveDatev()
